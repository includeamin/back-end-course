from flask import Blueprint, request

from Bridges.Auth import is_login
from Classes.User import User

user_route = Blueprint("User", __name__)


@user_route.route("/user/login/<phonenumber>", methods=["GET"])
def user_login(phonenumber):
    return User.login(user_mobile=phonenumber)


@user_route.route("/user/logout", methods=["GET"])
def user_logout():
    pass


@user_route.route("/user/info", methods=["GET"])
def user_get_info():
    is_login(request.headers)
    return User.get_user_info()

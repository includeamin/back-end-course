import json

import requests


def system_users_authentication_login(user_id):
    result = requests.post("http://localhost:3001/system/users/authentication/login", data={"Id": user_id}).content
    print(result)
    result = json.loads(result)
    result = result["Description"]["Token"]
    print(result)
    return result


def is_login(headers):
    user_id = headers["Id"]
    user_token = headers["Token"]
    result = requests.get("http://localhost:3001/system/users/authentication/check",
                          headers={"Id": user_id, "Token": user_token}).content
    result = json.loads(result)
    if result["Description"] != "accepted":
        raise Exception("Login Required")


def system_users_authentication_logout(user_id, user_token):
    result = requests.get("http://loccalhost:3001/system/users/authentication/logout").content
    result = json.load(result)
    return result

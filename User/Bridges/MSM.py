import json

import requests


def get_by_name(service_name):
    # service_name = "UserService"
    result = requests.get(f"http://localhost:3000/system/services/{service_name}/info").content
    print(result)
    result = json.loads(result)
    return result

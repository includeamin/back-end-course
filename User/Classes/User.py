from random import randint

from bson import ObjectId
from flask import request
from twilio.rest import Client

from Bridges.Auth import is_login
from Bridges.Bridge import send_auth_code
from Classes.Tools import Tools
from DataBase.DB import user_collection, code_collection
from Models.CodeModel import CodeModel
from Models.UserModel import UserModel, UserModelVN


class User:

    @staticmethod
    def gen_code(phone_number, user_id):

        code = randint(10000, 99999)
        code_collection.insert_one(CodeModel(**{"Code": int(code),
                                                "UserId": str(user_id),
                                                "PhoneNumber": phone_number,
                                                "Amin": "Amin"}))
        send_auth_code(phone_number, code)

    @staticmethod
    def login(user_mobile):
        try:
            same_user = user_collection.find_one({UserModelVN.Phonenumber: user_mobile})
            if same_user:
                # todo : send code
                User.gen_code(user_mobile, str(same_user.inserted_id))
                return Tools.Result(True, "Code has been sent")
            else:
                new_user = UserModel(Phonenumber=user_mobile)
                new_user = user_collection.insert_one(new_user.dict())
                # Token ???
                # todo : send code
                User.gen_code(user_mobile, str(new_user.inserted_id))
                return Tools.Result(True, "Code has been sent")
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def verify_phone_number(phone_number, code):
        try:
            client = Client()
            client.lookups.phone_numbers(phone_number).fetch(type="carrier")
            return Tools.Result(True, "True")
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def logout(user_id, user_token):
        try:
            pass
            # if not ObjectId.is_valid(user_id):
            #     raise Exception("Invalid Id")
            #
            # if system_users_authentication_check(user_id, user_token)["Description"] == "Exists":
            #     same_user = user_collection.find_one({"_id": ObjectId(user_id)})
            #     if not same_user:
            #         raise Exception("This user not exists")
            #     return system_users_authentication_logout(user_id, user_token)
            # else:
            #     raise Exception("Permission denied")
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def get_user_info():
        try:
            is_login(request.headers)

            # if not ObjectId.is_valid(user_id):
            #     raise Exception("Invalid Id")
            #
            # if system_users_authentication_check(user_id, user_token):
            #     same_user = user_collection.find_one({"_id": ObjectId(user_id), UserModelVN.Token: user_token})
            #     if not same_user:
            #         raise Exception("This user not exist")
            #     return Tools.Result(True, same_user)
            # else:
            #     raise Exception("Permission denied")

        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def update_user_info(user_id, user_token, new_data):
        try:
            if not ObjectId.is_valid(user_id):
                raise Exception("Invalid id")

            # if system_users_authentication_check(user_id, user_token)["Description"] == "Exists":
            #     same_user = user_collection.find_one({"_id": ObjectId(user_id), UserModelVN.Token: user_token})
            #     if not same_user:
            #         raise Exception("This user not exist")
            #     else:
            #         user_collection.update_one({"_id": ObjectId(user_id)}, {
            #             "$set": {UserModelVN.Fname: new_data[UserModelVN.Fname],
            #                      UserModelVN.Lname: new_data[UserModelVN.Lname],
            #                      UserModelVN.Gender: new_data[UserModelVN.Gender]}})
            #        return Tools.Result(True, "Done")
            else:
                raise Exception("Permission denied")

        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def follow_user(user_id, user_token, new_data):
        try:
            pass
        except Exception as ex:
            return Tools.Result(False, ex.args)

from datetime import datetime

from pydantic import BaseModel


class CodeModel(BaseModel):
    Code: int
    UserId: str
    PhoneNumber: str
    IsUsed: bool = False
    Create_at: datetime = datetime.now()


class CodeModelVN:
    id: str = "_id"
    Code: str = "Code"
    UserId: str = "UserId"
    PhoneNumber: str = "PhoneNumber"
    IsUsed: str = "IsUsed"
    Create_at: str = "Create_at"

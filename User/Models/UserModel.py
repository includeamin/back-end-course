from datetime import datetime

from pydantic import BaseModel


class UserModel(BaseModel):
    Phonenumber: str
    Fname: str = "guest"
    Lname: str = "guest"
    Gender: str = "guest"
    State: str = "NotActive"
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class UserModelVN:
    id: str = "_id"
    Phonenumber: str = "Phonenumber"
    Fname: str = "Fname"
    Lname: str = "Lname"
    Gender: str = "Gender"
    State: str = "State"
    Create_at: str = "Create_at"
    Update_at: str = "Update_at"

from flask import Flask, jsonify
from flask_cors import CORS

from API.UserApi import user_route

app = Flask(__name__)
CORS(app)
app.register_blueprint(user_route)


@app.route("/")
def index():
    return jsonify({"ServiceName": "UserService"})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3002, debug=True)

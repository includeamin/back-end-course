import json

import pymongo

from Bridges.MSM import get_by_name

user_service_info = get_by_name("MessageService")

database_String = user_service_info["Description"]["DataBaseString"]

with open("./Configs/configs.json") as f:
    configs = json.load(f)

    mongodb = pymongo.MongoClient(database_String)
    database = mongodb[configs["DataBaseName"]]
    text_collection = database[configs["TextCollectionName"]]
    file_collection = database[configs["FileCollection"]]

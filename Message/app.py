from flask import Flask, jsonify

from API.TextApi import text_route

app = Flask(__name__)
app.register_blueprint(text_route)


@app.route("/")
def index():
    return jsonify({"ServiceName": "Message Service"})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3003, debug=True)

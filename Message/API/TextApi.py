from flask import Blueprint, request

from Bridges.Auth import is_login
from Classes.Text import Text
from Models.TextMessageModel import TextMessageModelVN

text_route = Blueprint("TextMessage", __name__)


@text_route.route("/user/message/text/add", methods=["POST"])
def add_text_message():
    is_login(request.headers)
    body = request.form
    header = request.headers
    return Text.add(header["Id"], body[TextMessageModelVN.ReceiverId],
                    body[TextMessageModelVN.Text], body[TextMessageModelVN.Date],
                    body[TextMessageModelVN.EventId])

import json

import requests

from Classes.Tools import Tools


def is_login(headers):
    try:
        user_id = headers["Id"]
        user_token = headers["Token"]
        result = requests.get("http://localhost:3001/system/users/authentication/check",
                              headers={"Id": user_id, "Token": user_token}).content
        result = json.loads(result)
        print(result)
        if result["Description"] != "accepted":
            raise Exception("Login Required")
    except Exception as ex:
        return Tools.Result(False, ex.args)

from datetime import datetime

from pydantic import BaseModel


class TextMessageModel(BaseModel):
    Sender_id: str
    Receiver_id: str
    Text: str
    Date: str
    Event_id: str
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class TextMessageModelVN:
    id: str = "_id"
    SenderId: str = "Sender_id"
    ReceiverId: str = "Receiver_id"
    Text: str = "Text"
    Date: str = "Date"
    EventId: str = "Event_id"
    Create_at: str = "Create_at"
    Update_at: str = "Update_at"

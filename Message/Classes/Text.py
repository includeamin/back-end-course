from Classes.Tools import Tools
from DataBase.DB import text_collection
from Models.TextMessageModel import TextMessageModel, TextMessageModelVN


class Text:

    @staticmethod
    def add(sender_id, receiver_id, text, date, event_id):
        try:
            same_text = text_collection.find_one(
                {TextMessageModelVN.SenderId: sender_id, TextMessageModelVN.ReceiverId: receiver_id,
                 TextMessageModelVN.Date: date, TextMessageModelVN.EventId: event_id}, {TextMessageModelVN.id})
            if same_text:
                raise Exception("Not allow to send message")
            new_text = TextMessageModel(Sender_id=sender_id, Receiver_id=receiver_id, Text=text, Date=date,
                                        Event_id=event_id).dict()
            new_text = text_collection.insert_one(new_text)
            return Tools.Result(True, str(new_text.inserted_id))
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def get(message_id):
        try:
            pass
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def edit(message_id, new_data):
        try:
            pass
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def delete(message_id):
        try:
            pass
        except Exception as ex:
            return Tools.Result(False, ex.args)

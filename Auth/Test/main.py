import requests

base_url = "http://localhost:3001"


def login():
    result = requests.post(f"{base_url}/system/users/authentication/login",
                           data={"Id": "5da5cc497ecbfd66ebdda04e",
                                 "Token": "5da5cc497ecbfd66ebdda04e5da5cc497ecbfd66ebdda04e"})
    if result.status_code == 200:
        print(result.content)
    else:
        print(result.content)


def check_login():
    # 5da5cc497ecbfd66ebdda04e
    # 5da5cc497ecbfd66ebdda04e5da5cc497ecbfd66ebdda04e
    result = requests.get(f"{base_url}/system/users/authentication/check",
                          headers={"Id": "5da5cc497ecbfd66ebdda04e",
                                   "Token": "5da5cc497ecbfd66ebdda04e5da5cc497ecbfd66ebdda04e"})
    print(result.content)


def check_logout():
    # 5da5cc497ecbfd66ebdda04e
    # 5da5cc497ecbfd66ebdda04e5da5cc497ecbfd66ebdda04e
    result = requests.get(f"{base_url}/system/users/authentication/logout",
                          headers={"Id": "5da5cc497ecbfd66ebdda04e",
                                   "Token": "5da5cc497ecbfd66ebdda04e5da5cc497ecbfd66ebdda04e"})
    print(result.content)


if __name__ == '__main__':
    check_login()

from datetime import datetime

from pydantic import BaseModel


class AuthModel(BaseModel):
    Id: str
    Token: str
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class AuthModelVN:
    id: str = "_id"
    Id: str = "Id"
    Token: str = "Token"

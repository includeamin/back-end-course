from flask import Blueprint, request

from Classes.Authentication import Authentication
from Classes.VariableHelber import FormVN

auth_route = Blueprint("Authentication", __name__)


@auth_route.route("/system/users/authentication/login", methods=["POST"])
def system_users_authentication_login():
    form = request.form
    return Authentication.login(form[FormVN.Id])


@auth_route.route("/system/users/authentication/check", methods=["GET"])
def system_users_authentication_check():
    header = request.headers
    return Authentication.check(header[FormVN.Id], header[FormVN.Token])


@auth_route.route("/system/users/authentication/logout", methods=["GET"])
def system_users_authentication_logout():
    header = request.headers
    return Authentication.logout(header[FormVN.Id], header[FormVN.Token])

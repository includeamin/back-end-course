import json

import pymongo

from Bridges.MSM import get_by_name

# todo :

authentication_service_info = get_by_name("AuthService")

database_string = authentication_service_info["Description"]["DataBaseString"]
print(database_string)

with open("./Configs/configs.json") as f:
    configs = json.load(f)

    mongodb = pymongo.MongoClient(database_string)
    database = mongodb[configs["DataBaseName"]]
    auth_collection = database[configs["AuthCollectionName"]]

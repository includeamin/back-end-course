import json

import requests


def get_by_name(service_name):
    # service_name = "AuthService"
    print("Point")
    result = requests.get(f"http://localhost:3000/system/services/{service_name}/info").content
    result = json.loads(result)
    print(result)
    return result

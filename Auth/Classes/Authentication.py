from bson import ObjectId

from Classes.Tools import Tools
from DataBase.DB import auth_collection
from Models.AuthModel import AuthModel, AuthModelVN
from uuid import uuid4


class Authentication:
    @staticmethod
    def token_gen():
        uuid = uuid4()
        return uuid

    @staticmethod
    def login(user_id):
        try:
            # todo : check login mechanism
            same_auth = auth_collection.find_one({AuthModelVN.Id: user_id}, {AuthModelVN.id})
            if same_auth:
                # delete
                raise Exception("User Already logged in")
            if not ObjectId.is_valid(user_id):
                raise Exception("Invalid Id")
            token = Authentication.token_gen()
            new_user = AuthModel(Id=user_id, Token=token).dict()
            auth_collection.insert_one(new_user)
            # todo : generate token
            res = {
                "Token": token
            }
            return Tools.Result(True, res)
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def check(user_id, user_token):
        try:
            if not ObjectId.is_valid(user_id):
                raise Exception("Invalid Id")
            same_auth = auth_collection.find_one({AuthModelVN.Id: user_id, AuthModelVN.Token: user_token})
            if not same_auth:
                raise Exception("Not accepted")
            return Tools.Result(True, "accepted")
        except Exception as ex:
            return Tools.Result(False, ex.args)

    @staticmethod
    def logout(user_id, user_token):
        try:
            if not ObjectId.is_valid(user_id):
                raise Exception("Invalid Id")
            same_auth = auth_collection.find_one({AuthModelVN.Id: user_id, AuthModelVN.Token: user_token})
            if not same_auth:
                raise Exception("User not exist")
            # {AuthModelVN.Id: user_id, AuthModelVN.Token: user_token}
            auth_collection.delete_one({AuthModelVN.id: same_auth[AuthModelVN.id]})
            return Tools.Result(True, "Logout")
        except Exception as ex:
            return Tools.Result(False, ex.args)

from flask import Flask, jsonify

from API.AuthenticationApi import auth_route

app = Flask(__name__)

app.register_blueprint(auth_route)


@app.route("/")
def index():
    return jsonify({"Service Name": "Authentication Service"})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3001, debug=True)

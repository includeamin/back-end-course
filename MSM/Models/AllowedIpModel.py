from datetime import datetime

from pydantic import BaseModel


class AllowedIpModel(BaseModel):
    AdminName: str
    IP: str
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class AllowedIpModelVn:
    id: str = "_id"
    AdminName: str = "AdminName"
    IP: str = "IP"
    Create_at: str = "Create_at"
    Update_at: str = "Update_at"

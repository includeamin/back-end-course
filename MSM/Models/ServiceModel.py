from datetime import datetime

from pydantic import BaseModel


class ServiceModel(BaseModel):
    ServiceName: str
    Url: str
    DataBaseString: str
    Port: str
    Configs: dict = None
    Create_at: datetime = datetime.now()
    Update_at: datetime = datetime.now()


class ServiceModelVN:
    id: str = "_id"
    ServiceName: str = "ServiceName"
    Url: str = "Url"
    DataBaseString: str = "DataBaseString"
    Port: str = "Port"
    Configs: str = "Configs"
    Create_at: str = "Create_at"
    Update_at: str = "Update_at"

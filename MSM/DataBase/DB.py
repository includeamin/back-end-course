import json

import pymongo

with open("./Configs/configs.json") as f:
    configs = json.load(f)

    mongodb = pymongo.MongoClient(configs["DataBaseString"])
    database = mongodb[configs["DataBaseName"]]
    service_collection = database[configs["ServiceCollectionName"]]
    allowed_ip_collection = database[configs["AllowedIpCollectionName"]]

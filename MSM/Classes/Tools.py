import json


class Tools:
    @staticmethod
    def dumps(data):
        return json.dumps(data, indent=4, sort_keys=True, default=str)

    @staticmethod
    def result(state, description):
        return Tools.dumps({"State": state, "Description": description})

    @staticmethod
    def is_valid_ip(ip):
        if ip == "1":
            return False
        else:
            return True

    @staticmethod
    def check_body(key_list: list, form):
        # todo: impl
        pass

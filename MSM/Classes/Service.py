import json

from bson import ObjectId

from Classes.Tools import Tools
from DataBase.DB import service_collection
from Models.ServiceModel import ServiceModel, ServiceModelVN


class Service:

    @staticmethod
    def add(service_name, url, database, port):
        try:
            # todo: check if Ip has access or not
            same_service = service_collection.find_one({ServiceModelVN.ServiceName: service_name}, {ServiceModelVN.id})
            if same_service:
                raise Exception("This service is exists")
            new_service = ServiceModel(ServiceName=service_name, Url=url, DataBaseString=database, Port=port).dict()
            new_service = service_collection.insert_one(new_service)
            return Tools.result(True, {"ServiceId": str(new_service.inserted_id)})
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def get(service_name):
        try:
            same_service = service_collection.find_one({ServiceModelVN.ServiceName: service_name})
            if not service_name:
                raise Exception("This service not exists")
            return Tools.result(True, same_service)
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def get_all():
        try:
            same_service = service_collection.find()
            if not same_service:
                raise Exception("There is not any service")
            Tools.result(True, same_service)
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def update(service_id, new_data):
        try:
            # todo: check if Ip has access or not
            print(new_data)
            new_data = json.loads(new_data)
            same_service = service_collection.find_one({ServiceModelVN.id: ObjectId(service_id)})
            if not same_service:
                raise Exception("This service not exists")
            service_collection.update_one({ServiceModelVN.id: same_service[ServiceModelVN.id]},
                                          {"$set": {ServiceModelVN.ServiceName: new_data[ServiceModelVN.ServiceName],
                                                    ServiceModelVN.Url: new_data[ServiceModelVN.Url],
                                                    ServiceModelVN.DataBaseString: new_data[
                                                        ServiceModelVN.DataBaseString],
                                                    ServiceModelVN.Port: new_data[ServiceModelVN.Port]}})
            return Tools.result(True, "Done")
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def delete(service_id):
        try:
            # todo: check if Ip has access or not
            same_service = service_collection.find_one({ServiceModelVN.id: ObjectId(service_id)})
            if not same_service:
                raise Exception("This service not exists")
            service_collection.delete_one({ServiceModelVN.id: same_service[ServiceModelVN.id]})
            return "Done"
        except Exception as ex:
            Tools.result(False, ex.args)

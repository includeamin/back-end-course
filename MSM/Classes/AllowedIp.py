from bson import ObjectId

from Classes.Tools import Tools
from DataBase.DB import allowed_ip_collection
from Models.AllowedIpModel import AllowedIpModel, AllowedIpModelVn


class AllowedIp:
    @staticmethod
    def add(admin_name, ip):
        try:
            if not Tools.is_valid_ip(ip):
                raise Exception("Invalid Ip")
            same_admin = allowed_ip_collection.find_one({AllowedIpModelVn.AdminName: admin_name}, {AllowedIpModelVn.id})
            if same_admin:
                raise Exception("This admin name already exist!")
            new_admin = AllowedIpModel(AdminName=admin_name, Ip=ip).dict()
            new_admin = allowed_ip_collection.insert_one(new_admin)
            return Tools.result(True, {"NewAdminId": str(new_admin.inserted_id)})
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def get(admin_id):
        try:
            if not ObjectId.is_valid(admin_id):
                raise Exception("Invalid admin id!")
            same_admin = allowed_ip_collection.find_one({AllowedIpModelVn.id: ObjectId(admin_id)})
            if not same_admin:
                raise Exception("This admin not exist!")
            return Tools.result(True, same_admin)
        except Exception as ex:
            return Tools.result(False, ex)

    @staticmethod
    def update(admin_id, new_data):
        try:
            if not ObjectId.is_valid(admin_id):
                raise Exception("Invalid admin id!")
            same_admin = allowed_ip_collection.find_one({AllowedIpModelVn.id: ObjectId(admin_id)})
            if not same_admin:
                raise Exception("This admin not exist!")
            allowed_ip_collection.update_one({AllowedIpModelVn.id: same_admin[AllowedIpModelVn.id]},
                                             {"$set": {AllowedIpModelVn.AdminName: new_data[AllowedIpModelVn.AdminName],
                                                       AllowedIpModelVn.IP: new_data[AllowedIpModelVn.IP]}})
            return Tools.result(True, "done")

        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def delete(admin_id):
        try:
            if not ObjectId.is_valid(admin_id):
                raise Exception("Invalid admin id!")
            same_admin = allowed_ip_collection.find_one({AllowedIpModelVn.id: ObjectId(admin_id)})
            if not same_admin:
                raise Exception("This admin not exist!")
            allowed_ip_collection.delete_one({AllowedIpModelVn.id: ObjectId(admin_id)})
            return Tools.result(True, "done")
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def is_allowed(admin_ip):
        try:
            if not Tools.is_valid_ip(admin_ip):
                raise Exception("Invalid Ip")
            same_admin = allowed_ip_collection.find_one({AllowedIpModelVn.IP: admin_ip})
            if not same_admin:
                raise Exception("False")
            return Tools.result(True, "True")
        except Exception as ex:
            return Tools.result(False, ex.args)

    @staticmethod
    def admin_get_all_admins():
        try:
            same_admin = allowed_ip_collection.find({})
            if not same_admin:
                raise Exception("There is not any admin")
            return Tools.result(True, same_admin)
        except Exception as ex:
            return Tools.result(False, ex.args)

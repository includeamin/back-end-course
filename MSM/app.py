from flask import Flask
from flask import jsonify

from API.ServiceApi import service_route

app = Flask(__name__)

app.register_blueprint(service_route)


@app.route("/")
def index():
    return jsonify({"ServiceName": "MicroService Management"})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=True)

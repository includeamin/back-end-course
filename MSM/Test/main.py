import json

import requests


def add_service():
    service_name = "MessageService"
    service_database = "mongodb://root:Aaw7K8aaOkZPMs8DMQfEToeQ@s8.liara.ir:31977"
    service_url = "http://localhost:3003/"
    service_port = 3003
    result = requests.post("http://localhost:3000/admin/service/add",
                           data={"ServiceName": service_name,
                                 "DataBaseString": service_database,
                                 "Url": service_url,
                                 "Port": service_port}).content
    print(result)


def get_by_name():
    service_name = "AuthService"
    result = requests.get(f"http://localhost:3000/system/services/{service_name}/info").content
    result = json.loads(result)
    print(result)


def update_service():
    service_name = "AuthService"
    new_data = {
        "ServiceName": "AuthService",
        "Port": 3002,
        "Url": "http://localhost:3002",
        "DataBaseString": "mongodb://root:xxx@s8.liara.ir:31977"
    }
    new_data = json.dumps(new_data)
    result = requests.post(f"http://localhost:3000/admin/service/{'5da3341bcde75c1dae2c10c9'}/update",
                           data={"NewData": new_data}).content
    print(result)


def delete_service():
    result = requests.get("http://localhost:3000/admin/service/5da3341bcde75c1dae2c10c9/delete").content
    print(result)


if __name__ == '__main__':
    add_service()

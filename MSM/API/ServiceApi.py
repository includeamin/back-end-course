from flask import Blueprint, request

from Classes.Service import Service
from Models.ServiceModel import ServiceModelVN

service_route = Blueprint("Service", __name__)


@service_route.route("/admin/services", methods=["GET"])
def admin_service_index():
    # todo : get all services
    # TODO :CHECK ADMIN
    return Service.get_all()


@service_route.route("/admin/service/add", methods=["POST"])
def admin_add_service():
    # key_list = ["ServiceName",
    #             "DataBaseString",
    #             "Url",
    #             "Port"]
    #
    form: dict = request.form
    #
    # for item in key_list:
    #     if not form.keys().__contains__(item):
    #         return f"{item} not exist in the body"

    service_name = form[ServiceModelVN.ServiceName]
    database_string = form[ServiceModelVN.DataBaseString]
    service_url = form[ServiceModelVN.Url]
    service_port = form[ServiceModelVN.Port]
    return Service.add(service_name, service_url, database_string, service_port)


@service_route.route("/system/services/<service_name>/info")
def system_get_service_configs(service_name):
    # todo : check internal request
    return Service.get(service_name=service_name)


@service_route.route("/admin/service/<service_id>/update", methods=["POST"])
def admin_update_service(service_id):
    form = request.form
    return Service.update(service_id, form["NewData"])


@service_route.route("/admin/service/<service_id>/delete")
def admin_service_delete(service_id):
    return Service.delete(service_id)
